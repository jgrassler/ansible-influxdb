# InfluxDB

Install [InfluxDB](http://influxdb.org/) time series database

## Role Variables

| Name                        | Default Value | Description                                                      |
|-----------------------------|---------------|------------------------------------------------------------------|
| influxdb_client_port        | 8086          | The port for influxdb client connections                         |
| influxdb_ssl_certificate    | None          | If defined the influxdb_client_port will be set to SSL           |
| influxdb_ssl_certificate_src| None          | If defined the file at this location wil be copied to the host   |
| influxdb_use_apt            | false         | If true apt will be used to install influxdb                     |
| influxdb_deb_src_url        | http://s3.amazonaws.com/influxdb/ | If not using apt the url base to pull the deb from |

## Optional
- influxdb_bind_address - address to be used for binding the influxdb ports. Default is to bind to all IP addresses.
- run_mode - One of Deploy, Stop, Install, Start, or Use. The default is Deploy which will do Install, Configure, then Start.
- influxdb_http_log_enabled - Should influxdb log http requests? Default to true.
- influxdb_retention_enabled - Is retention enabled? Defaults to true.
- influxdb_admin_enabled - Is admin module enabled? Defaults to true.
- influxdb_http_enabled - Is http module enabled? Defaults to true.
- influxdb_monitoring_enabled - Is monitoring module enabled? Defaults to true.influxdb_monitoring_enabled.
- influxdb_participates_raft_group - Controls if node should run the metaservice and participate in the Raft group.
- influxdb_holds_data_shards - Controls if node holds time series data shards in the cluster.
- influxdb_query_log_enabled - Whether queries should be logged before execution.
- influxdb_raft_port - Port number for binding meta data. Please do not change
  this port number. InfluxDB during recovery uses hard coded port number which
  is 8088.

## License

MIT
